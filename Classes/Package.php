<?php

namespace Clicsoft\Workshop;

use Clicsoft\Workshop\Policy\PolicyDataProvider;
use Neos\Flow\Configuration\ConfigurationManager;
use Neos\Flow\Core\Bootstrap;
use Neos\Flow\Package\Package as BasePackage;

/**
 * Author: peach
 * Date: 02.11.17
 */
class Package extends BasePackage
{
    
    /**
     * Register PolicyExtension as custom configuration type.
     *
     * @param Bootstrap $bootstrap
     * @return void
     */
    public function boot(Bootstrap $bootstrap)
    {
        $dispatcher = $bootstrap->getSignalSlotDispatcher();
        $dispatcher->connect(ConfigurationManager::class, 'configurationManagerReady',
            function (ConfigurationManager $configurationManager) {
                $configurationManager->registerConfigurationType(
                    PolicyDataProvider::CONFIGURATION_TYPE_POLICY_EXTENSION,
                    ConfigurationManager::CONFIGURATION_PROCESSING_TYPE_POLICY,
                    true);
            }
        );
    }
    
}
<?php

namespace SportManager\Security\Policy;

/*
 * This file is part of the SportManager.Security package.
 */

use Neos\Flow\Annotations as Flow;

/**
 * A representation of a role defined in a Policy.yaml file and extended in a PolicyExtension.yaml file.
 */
class Role
{
    
    /**
     * @var string
     */
    protected $identifier;
    
    /**
     * @var array
     */
    protected $attributes;
    
    /**
     * @var Role[]
     */
    protected $parentRoles;
    
    /**
     * @var \Neos\Flow\Security\Policy\Role
     */
    protected $rawRole;
    
    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }
    
    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;
    }
    
    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }
    
    /**
     * @return Role[]
     */
    public function getParentRoles()
    {
        return $this->parentRoles;
    }
    
    /**
     * @param Role[] $parentRoles
     */
    public function setParentRoles(array $parentRoles)
    {
        $this->parentRoles = $parentRoles;
    }
    
    /**
     * @return \Neos\Flow\Security\Policy\Role
     */
    public function getRawRole(): \Neos\Flow\Security\Policy\Role
    {
        return $this->rawRole;
    }
    
    /**
     * @param \Neos\Flow\Security\Policy\Role $rawRole
     */
    public function setRawRole(\Neos\Flow\Security\Policy\Role $rawRole)
    {
        $this->rawRole = $rawRole;
    }
    
}
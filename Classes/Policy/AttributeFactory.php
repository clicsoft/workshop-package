<?php

namespace Clicsoft\Workshop\Policy;

use Neos\Flow\Annotations as Flow;

/**
 * Author: Peter Gisler
 * Date: 31.10.17
 */
class AttributeFactory
{
    
    /**
     * @Flow\InjectConfiguration(type="PolicyExtension", path="attributes")
     * @var array
     */
    protected $attributeConfiguration;
    
    /**
     * Factory method for building objects of type Clicsoft\Workshop\Attribute.
     *
     * @param string $identifier Unique identifier of attribute
     * @return Attribute
     * @throws \Exception
     */
    public function create(string $identifier): Attribute
    {
        $attribute = new Attribute();
        $attribute->setIdentifier($identifier);
        
        if (array_key_exists($identifier, $this->attributeConfiguration)) {
            $attributeConfiguration = $this->attributeConfiguration[$identifier];
            if (!array_key_exists('attributeType', $attributeConfiguration)) {
                throw new \Exception("Missing attribute type definition for attribute '{$identifier}'.", 1509460166);
            }
            $attribute->setType($attributeConfiguration['attributeType']);
        }
        
        return $attribute;
    }
}
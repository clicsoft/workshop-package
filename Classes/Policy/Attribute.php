<?php

namespace Clicsoft\Workshop\Policy;

use Neos\Flow\Annotations as Flow;

/**
 * A representation of an attribute described in a PolicyExtension.yaml file.
 */
class Attribute
{
    
    /**
     * @var string
     */
    protected $identifier;
    
    /**
     * @Flow\Validate(type="NotEmpty")
     * @var string
     */
    protected $type;
    
    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }
    
    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier)
    {
        $this->identifier = $identifier;
    }
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }
}

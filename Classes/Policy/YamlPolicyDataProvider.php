<?php

namespace Clicsoft\Workshop\Policy;

use Doctrine\Common\Collections\ArrayCollection;
use Neos\Flow\Annotations as Flow;
use Doctrine\Common\Collections\Collection;
use Neos\Flow\Configuration\ConfigurationManager;
use Neos\Flow\ObjectManagement\ObjectManagerInterface;
use SportManager\Security\Policy\Role;

/**
 * Author: peach
 * Date: 31.10.17
 *
 * @Flow\Scope("singleton")
 */
class YamlPolicyDataProvider implements PolicyDataProvider
{
    
    /**
     * @var array
     */
    protected $configuration;
    
    /**
     * @Flow\Inject
     * @var ObjectManagerInterface
     */
    protected $objectManager;
    
    /**
     * YamlPolicyDataProvider constructor.
     *
     * Populates the configuration property of the yaml policy data provider with the merged PolicyExtension
     * configuration.
     */
    public function __construct(ConfigurationManager $configurationManager)
    {
        $this->configuration = $configurationManager->getConfiguration(PolicyDataProvider::CONFIGURATION_TYPE_POLICY_EXTENSION);
    }
    
    public function getRoles(): Collection
    {
        $roles = new ArrayCollection();
        foreach ($this->configuration['roles'] as $identifier => $roleConfiguration) {
            $roles->add($this->objectManager->get(Role::class, $identifier));
        }
        
        return $roles;
    }
    
}
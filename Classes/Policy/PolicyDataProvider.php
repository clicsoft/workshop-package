<?php

namespace Clicsoft\Workshop\Policy;

/**
 * Author: Peter Gisler
 * Date: 17.10.17
 */

use Doctrine\Common\Collections\Collection;

/**
 * Interface PolicyDataProvider
 *
 * The policy data provider is responsible for reading, inflating and returning policy related data.
 * This policy related data mainly consists in roles, attributes and attribute values.
 *
 * @package SportManager\Security\Policy
 */
interface PolicyDataProvider
{
    
    const CONFIGURATION_TYPE_POLICY_EXTENSION = 'PolicyExtension';
    
}
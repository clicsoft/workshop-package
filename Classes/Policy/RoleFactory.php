<?php

namespace SportManager\Security\Policy;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\ObjectManagement\ObjectManagerInterface;

/**
 * Author: peach
 * Date: 31.10.17
 */
class RoleFactory
{
    
    /**
     * @Flow\Inject
     * @var ObjectManagerInterface
     */
    protected $objectManager;
    
    /**
     * @Flow\InjectConfiguration(type="PolicyExtension", path="roles")
     * @var array
     */
    protected $roleConfiguration;
    
    /**
     * Factory method for building objects of type Clicsoft\Workshop\Role.
     *
     * @param string $identifier Role identifier
     * @return Role
     */
    public function create(string $identifier): Role
    {
        $role = new Role();
        $role->setIdentifier($identifier);
        $role->setRawRole(new \Neos\Flow\Security\Policy\Role($identifier));
        
        $role->setParentRoles($this->getParentRoles($role));
        
        if (array_key_exists($identifier, $this->roleConfiguration)) {
            $roleConfiguration = $this->roleConfiguration[$identifier];
            
            if (array_key_exists('attributes', $roleConfiguration)) {
                $attributes = [];
                foreach ($roleConfiguration['attributes'] as $attributeIdentifier) {
                    $attributes[] = $this->objectManager->get(Attribute::class, $attributeIdentifier);
                }
            }
        }
        
        return $role;
    }
    
    /**
     * Adds an array of Clicsoft\Workshop\Role objects as direct parent roles.
     * The source for retrieving the direct parent roles is the rawRole.
     *
     * @param Role $role The role for which the direct parent roles should be retrieved
     * @return array
     */
    private function getParentRoles(Role $role)
    {
        $parentRoles = [];
        foreach ($role->getRawRole()->getParentRoles() as $rawParentRole) {
            $parentRoles[] = $this->create($rawParentRole->getIdentifier());
        }
        
        return $parentRoles;
    }
    
}
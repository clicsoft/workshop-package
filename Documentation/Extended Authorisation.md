# Daten für die erweiterte Autorisierung
Informationen für die Authentifizierung und Autorisierung werden in verschiedenen Konfigurationsfiles definiert. Für die Benutzerverwaltung in der Applikation müssen diese Informationen zur Laufzeit in aufbereiteter Form zur Verfügung stehen.

## Inhalte der Konfigurationsdateien
Das Framework Flow sieht die Datei `Configuration/Policy.yaml` als zentraler Ort für die Definition aller Informationen für die Autorisierung von Benutzern vor. Konkret werden in dieser Datei **Rollen** und **Rechte** definiert. Zudem werden die definierten Rechte ausgewählten Rollen zugewiesen.

### Limitationen durch das Framework Flow
Für die benutzergerechte Aufbereitung aller Informationen sind neben technischen Bezeichnungen und Beschreibungen zusätzliche Informationen erforderlich. Diese Informationen können _nicht_ in der Datei `Policy.yaml` des Frameworks Flow gespeichert werden.

Es existiert ein [Schema für die Policy-Datei](https://github.com/neos/flow/blob/master/Resources/Private/Schema/Policy.schema.yaml), welche abschliessend alle erlaubten Elemente der `Policy.yaml` Datei beschreibt. Dieses Schema lässt keine Ergänzungen zu den frameworkspezifischen Inhalten zu.

### Applikationsspezifische Erweiterungen
Für die Darstellung und Interpretation der Autorisierungs-Informationen zur Benutzerverwaltung innerhalb der Applikation werden zusätzliche Informationen benötigt. Insbesondere muss die bestehende _Role-Based Access Control_ (RBAC), beruhend auf Rollen und Rechten, um **Attribute** ergänzt werden. 

Da eine Erweiterung der Datei `Policy.yaml` nicht möglich bzw. sinnvoll ist, werden zusätzlich benötigte Informationen in der Datei `Configuration/PolicyExtension.yaml` definiert.

In den nachfolgenden Abschnitten sind diese zusätzlich benötigten Informationen beschrieben.

#### Attribute
Attribute schränken die Rechte einer Rolle auf einen domänenspezifischen Bereich ein. Zur korrekten Interpretation eines korrekten Attributwertes ist für jedes Attribut die Angabe eines Typs erforderlich. Dieser Typ kann ein primitiver  Datentyp oder eine Klasse sein.

#### Sprachneutrale Identifier für die Übersetzung
Zur Anzeige von Informationen in der Benutzerverwaltung muss es möglich sein, Rollen, Rechte und Attribute angemessen bezeichnet und beschrieben darzustellen. Es werden deshalb Übersetzungs-Identifier für diese Elemente definiert. Dabei ist für jedes Element sowohl ein Titel-Identifier als auch ein Beschreibungs-Identifier erforderlich.

### Beispiel-Konfiguration `PolicyExtension.yaml`
Nachfolgend ist beispielhaft der Inhalt einer `PolicyExtension.yaml` Datei abgebildet.

```yaml
#
# ATTRIBUTES
#
# Define attributes with name, type and localization specific data
attributes:
  number:
    type: 'int'
    localization:
      titleId: 'attributes.number.title'
      descriptionId: 'attributes.number.description'
      package: 'SportManager.Security'  # since this is the default value, it's not required to define here
      source: 'PolicyExtension.xlf'     # since this is the default value, it's not required to define here
  club:
	type: 'SportManager\Indoorvolleyball\Domain\Model\Club'
    localization:
      titleId: 'attributes.club.title'
      descriptionId: 'attributes.club.description'

#
# ROLES
#
# Roles with their parentRoles and privilegeTargets are defined in Policy.yaml.
# Here additional properties for roles are defined: attributes and localization specific data
roles:
  SportManager.Playground:User:
	attributes: ['club']
    localization:
      titleId: 'roles.sportManager.playground.user.title'
      descriptionId: 'roles.sportManager.playground.user.description'

#
# Privilege targets
#
# Privilege targets are defined in Policy.yaml.
# Here only localization specific data is defined for privilege targets.
privilegeTargets:
  SportManager.Playground:StandardController.indexAction:
    localization:
	  titleId: 'privilegeTargets.sportManager.playground.standardController.indexAction.title'
	  descriptionId: 'privilegeTargets.sportManager.playground.standardController.indexAction.description'
```

## Inhalte der Datenbank
Die vorhergehenden Informationen sind statische Informationen, welche in Konfigurationsdateien definiert werden. Die dynamischen, benutzerspezifischen Daten - Accounts (inkl. Rollen) und Attributwerte - werden dagegen in der Datenbank gespeichert.

### Accounts
Zur Speicherung der Accounts wird das Objekt `\Neos\Flow\Security\Account` verwendet. Dadurch werden Accounts bei der Persistierung durch das entsprechende Repository automatisch in die Datenbank-Tabelle `neos_flow_security_account` gespeichert. Die Assoziation zwischen Account und Rolle wird als kommasperarierte Liste von Rollen innerhalb des Attributs `roleIdentifiers` persistiert.

## Verwendung Daten aus `PolicyExtension.yaml` in der Applikation
Daten aus der Policy-Erweiterung `PolicyExtension.yaml` werden bei der Benutzerverwaltung benötigt. Es muss deshalb eine Möglichkeit existieren, zur Laufzeit auf die aufbereiteten Inhalte dieser Konfigurations-Datei zugreifen zu können.

Grundsätzlich sind unter Beachtung einer [SSOT](https://de.wikipedia.org/wiki/SPOT_(Datenmanagement) "Single Source of Truth") dafür zwei verschiedene Methoden denkbar:
1) Die Inhalte der Konfigurations-Dateien werden in der Datenbank abgebildet
2) Auf die Inhalte der Konfigurations-Dateien wird direkt zugegriffen

Aus Gründen der Effizient und um allfällige Synchronisationsprobleme zwischen Konfigurations-Dateien und Datenbank zu vermeiden, wird der zweite Ansatz gewählt. Es wird bei der Implementation jedoch darauf geachtet, den Zugriff auf die relevanten Daten soweit wie möglich zu abstrahieren. Dadurch soll die Möglichkeit erhalten bleiben, zu einem späteren Zeitpunkt die gewählte Methode ggf. anpassen bzw. austauschen zu können.
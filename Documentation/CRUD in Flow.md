# Ziel: CRUD Rechte für Objekte und deren Attribute definieren
Es gibt Situationen, in welchen Benutzer mit unterschiedlichen Rollen verschiedene Berechtigungen zur Einsicht bzw. Mutation von Objekten haben.

__Beispiele:__

+ Objekt _Spiel_:
  + Verein:
    + Sehen: Nur die Spiele, an welchen eines seiner Teams beteiligt ist
    + Mutieren: Nur die Attribute _Halle_ und _Anspielzeit_
  + Meisterschaftsbetreiber
    + Sehen: Nur die Spiele, welche er als Meisterschaftsbetreiber überwacht
    + Mutieren: Zusätzlich die Attribute _Schiedsrichter_, _Sichtbarkeit_
+ Objekt _Schiedsrichter_:
  + Schiesrichter:
    + Sehen: Nur sich selbst
    + Mutieren: Nur Stammdaten und persönliche Einstellungen (Verfügbarkeit, Stammverein, aktive Teams, ...)
  + SR-Verantwortlicher des Meisterschaftsbetreibers:
    + Sehen: Alle Schiedsrichter seiner Meisterschaft
    + Mutieren: Zusätzlich die Attribute _Qualifikation_, _Einschränkung auf Geschlecht_, _Ausgeschlossene Vereine_, ...
   
Es soll deshalb ein System entwickelt werden, in welchem die CRUD (Create, Read, Update, Delete) Rechte für Benutzer auf den zwei Ebenen _Objekt_ und _Attribut_ geregelt werden können.

## Voraussetzungen
Mit der _EntityPrivilege_ stellt Flow bereits eine Möglichkeit zur Verfügung, um das Auslesen von Entities (Objekten) aus der Datenbank zu steuern. Durch die Definition einer EntityPrivilege wird der Zugriff auf diese Resource geschützt. Es können danach nur noch Benutzer auf diese geschützte Entity zugreifen, welche eine Rolle mit spezifischer Berechtigtigung für diese EntityPrivilege haben.

Diese Einschränkung regelt den Zugriff auf geschützte Entities auf der Ebene der Datenbank. Bei fehlender Berechtigung werden die geschützten Entities gar nicht erst aus der Datenbank geladen. Es ist folglich keine der vier CRUD Operationen für diese geschützten Objekte möglich. Falls der Benutzer berechtigt ist, ein geschütztes Objekt aus der Datenbank zu laden, ist durch die EntityPrivilege keine weitere Möglichkeit vorhanden, die Berechtigungen spezifischer zu gestalten (in Bezug auf die Operationen Create, Update, Delete).

*Die _EntityPrivilege_ ermöglicht es folglich, die Sichtbarkeit (Read-Operation) auf Ebene _Objekt_ zu regeln. Bei fehlendem Recht, ein Objekt zu sehen, kann dieses konsequenterweise auch bearbeitet oder gelöscht werden. Die Erstellung eines Objekt des spezifischen Typs ist nicht weiter geregelt.*

## Anforderungen
Die nachfolgenden Anforderungen müssen erfüllt werden:

### Objekte
1. Das Recht zur Erstellung (Create) von Objekten eines spezifischen Typs kann definiert und mindestens einer Rolle zugewiesen werden.
2. Das Recht zur Bearbeitung (Update) von Objekten eines spezifischen Typs kann in Abhängigkeit von konkreten Eigenschaften des Objekts definiert und mindestens einer Rolle zugewiesen werden.
3. Das Recht zur Löschung (Delete) von Objekte eines spezifischen Typs kann in Abhängigkeit von konkreten Eigenschaften des Objekts definiert und mindestens einer Rolle zugewiesen werden.

### Attribute
1. Das Recht zur Sichtbarkeit (Read) eines Attributs eines spezifischen Typs kann in Abhängigkeit von konkreten Eigenschaften des Objekts definiert und mindestens einer Rolle zugewiesen werden.
2. Das Recht zur Bearbeitung (Update) von Objekten eines spezifischen Typs kann in Abhängigkeit von konkreten Eigenschaften des Objekts definiert und mindestens einer Rolle zugewiesen werden.

## Umsetzungsideen
Die nachfolgende Auflistung spielgelt grundlegende Ideen zur Umsetzung der oben genannten Anforderungen wieder. Diese Ideen sind _nicht_ als fertiges Konzept zur Implementation zu verstehen.

+ Es werden 5 Typen von Privilege Targets, entsprechend den obigen Anforderungen, definiert:
  + __ObjectCreatePrivilege__: Eingriff auf SQL-Ebene (INSERT)
  + __ObjectUpdatePrivilege__: Eingriff auf SQL-Ebene (UPDATE)
  + __ObjectDeletePrivilege__: Eingriff auf SQL-Ebene (DELETE)
  + __AttributeReadPrivilege__: Eingriff auf Domain Model Ebene (Getter-Methoden)
  + __AttributeUpdatePrivilege__: Eingriff auf Domain Model Ebene (Setter-Methoden)
+ Für die Definition der jeweiligen Privileges wird die Syntax analog des bereits bestehenden [EntityPrivilege](http://flowframework.readthedocs.io/en/stable/TheDefinitiveGuide/PartIII/Security.html#content-security-entityprivilege "Content Security in Flow Framework") matchers verwendet

### Objekte
In diesem Abschnitt werden konkrete Umsetzungsideen für Privilege Targets auf der Ebene von ganzen Objekten beschrieben.

Grundsätzlich kann für das Match-Statement dieser Privilege Targets eine [EEL Expression](http://flowframework.readthedocs.io/en/stable/TheDefinitiveGuide/PartIII/Eel.html "Embedded Expression Langauge") verwendet werden. Die _Embedded Expression Language_ erlaubt es, Statements in domänenspezifischer Sprache zu formulieren.

#### ObjectCreatePrivilege
Nachfolgend sind verschiedene Beispiele zu sehen, wie das Erstellen eines Objects vom Typ `SportManager\Playground\Domain\Model\Book` geschützt werden kann.

```yaml
privilegeTargets:
  SportManager\Playground\Security\Authorization\Privilege\Object\ObjectCreatePrivilege:
    'SportManager.Playground:CreateBooks':
      matcher: 'isType("SportManager\Playground\Domain\Model\Book")'

    'SportManager.Playground:CreateExpensiveBooks':
      matcher: 'isType("SportManager\Playground\Domain\Model\Book") && property(expensive) == TRUE'

    'SportManager.Playground:CreateOtherAuthorsBooks':
      matcher: 'isType("SportManager\Playground\Domain\Model\Book") && !(property("author").equals("context.securityContext.account"))'
```

#### ObjectUpdatePrivilege
tbd

#### ObjectDeletePrivilege
tbd

### Attribute
In diesem Abschnitt werden konkrete Umsetzungsideen für Privilege Targets auf der Ebene von Objekt-Attributen beschrieben.

#### AttributeReadPrivilege
tbd

#### AttributeUpdatePrivilege
tbd
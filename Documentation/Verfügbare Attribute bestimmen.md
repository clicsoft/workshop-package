﻿# Verfügbare Attribute eines bestimmten Typs evaluieren
In diesem Dokument wird das strukturierte Vorgehen zur Bestimmung einer endlichen Menge von Werten eines spezifischen Attribut-Typs beschrieben.
## Ein Beispiel zur Verdeutlichung der Ausgangslage
Mit dem nachfolgenden Beispiel soll die Ausgangslage verdeutlicht werden.

### Beschreibung des Beispiels
Berechtigte Benutzer eines Regionalverbandes können die Anmeldung eines neuen Vereins erfassen. Diese Anmeldung wird zur weiteren Bearbeitung an Swiss Volley weitergeleitet.

Ein wesentliches Attribut eines Vereins ist die eindeutige Zugehörigkeit zu einem Regionalverband. Im Verlauf der Erfassung eines neuen Vereins muss folglich diese Zugehörigkeit zu einem bestimmten Regionalverband durch den erfassenden Benutzer festgelegt werden.

Im einfachen Fall ist der Benutzer berechtigt Vereine genau für einen einzigen Regionalverband zu erfassen. In diesem Fall wird der entsprechende Regionalverband automatisch als Attribut des neu zu erstellenden Vereins festgelegt.

Im etwas komplexeren Fall ist der Benutzer berechtigt Vereine für verschiedene Regionalverbände zu erfassen. In diesem Fall muss der Benutzer im Prozess der Erfassung aller relevanter Daten für den neuen Verein mitunter einen Regionalverband als Wert für das entsprechende Attribut der Regionalverbands-Zugehörigkeit auswählen.

Die Aufgabe des Systems besteht darin alle validen Werte des spezifischen Attribut-Typs _Regionalverband_ zu bestimmen.

### Beispiel-Daten
Folgende Daten werden im weiteren Verlauf der Problembeschreibung verwendet:
|Objekt/Typ|Wert  |
|--|--|
|Recht|Ein PrivilegeTarget des Typs `Neos\Flow\Security\Authorization\Privilege\Method\MethodPrivilege` mit dem Identifier  `SportManager.IndoorVolleybal:ClubController.registerNewClub`|
|Rollen, die dieses Recht enthalten|`regionalAssociationAdministrator` und `regionalAssociationSecretary`|
|Identifier des Attributs|`regionalAssociation`|
|Typ des Attributs|`SportManager\Indoorvolleyball\Domain\Model\RegionalAssociation`|
|Werteraum des Attributs|`SVRA` (=Swiss Volley Region Aargau), `SVRI` (=Swiss Volley Region Aargau), `SVRZ` (=Swiss Volley Region Aargau), `SVRW` (=Swiss Volley Region Aargau), ...|

## Technische Betrachtung des Problems
Zur Laufzeit der Applikation soll eine Teilmenge des Werteraums eines spezifischen Attributs als endliche Menge zulässiger Werte dieses Attributs bestimmt werden.

Es gilt dabei verschiedene Eigenschaften der auszuführenden Aktion sowie des Benutzers zu beachten, um die Menge der zulässigen Attributswerte zu bestimmen.

### Strukturiertes Vorgehen
Die nachfolgenden Schritte zeigen das beabsichtige Vorgehen zur Lösung des beschriebenen Problems auf.
#### <a name="step1"></a>Attribut festlegen
Es muss festgelegt werden, welches Attribut in der folgenden Betrachtung von Interesse ist. Jedes Attribut kann eindeutig anhand eines Identifiers gemäss der Definition dieses Attributs in der Datei `PolicyExtension.yaml` bezeichnet werden.

_Wert aus dem Beispiel: `regionalAssociation`_

Für dies Attribut ist in der genannten Konfigurationsdatei eine Eigenschaft `attributeType` definiert. Der Wert dieser Eigenschaft gilt als Typ des Attributs.

> Ziel: Der Identifier und damit implizit auch der Typ des Attributs sind bekannt.

#### <a name="step2"></a>Relevantes Recht der Aktion bestimmen
Es kann davon ausgegangen werden, dass zum Zeitpunkt der Entwicklung jederzeit eindeutig bestimmt werden kann, welche Aktion (bzw. welche Methode) zur Bewältigung der jeweiligen Aufgabe ausgeführt wird. Es soll nun zur Laufzeit bestimmt werden, welche Privilege Targets die Ausführung dieser spezifischen Aktion schützen.

Es soll also anhand des Methodennamens und Klassennamens überprüft werden, welche Privilege Targets die Ausführung dieser Aktion schützen. Möglicherweise müssen auch aktuelle Parameter zur Laufzeit für die Evaluation der Privilege Targets berücksichtigt werden!

Ein möglicher Ausgangspunkt dafür kann der Abschnitt [Internal workings of method invocation authorization (Method Privilege)](http://flowframework.readthedocs.io/en/stable/TheDefinitiveGuide/PartIII/Security.html#internal-workings-of-method-invocation-authorization-methodprivilege) im Kapitel [Security](http://flowframework.readthedocs.io/en/stable/TheDefinitiveGuide/PartIII/Security.html) der offiziellen Flow Dokumentation sein.

_Wert aus dem Beispiel: `[SportManager.IndoorVolleybal:ClubController.registerNewClub]`_

> Ziel: Ein Array mit Privilege Targets, welche die Ausführung der aktuellen Aktion schützen, ist bekannt.

#### <a name="step3"></a>Relevante Rollen zur Ausführung der geschützten Aktion bestimmen
Basierend auf den zuvor evaluierten Privilege Targets soll nun eine Menge an Rollen bestimmt werden, welche einen Benutzer berechtigen, die geschützte Aktion auszuführen.

Dazu muss ein _Reverse Lookup_ implementiert werden, welcher ausgehend von jedem zuvor bestimmten Privilege Target eine endliche Menge an Rollen bestimmt. Die relevanten Informationen für dieses _Reverse Lookup_ sind in den `Policy.yaml` Konfigurationsdateien definiert.

Weil für die Bestimmung zulässiger Werte eines Attributs nur jene Rollen relevant sind, welche auch mit dem Typ dieses Attributs assoziiert sind, kann die Menge zuvor bestimmter Rollen möglicherweise entsprechend eingeschränkt werden.

_Wert aus dem Beispiel: `[regionalAssociationAdministrator, regionalAssociationSecretary]`_

> Ziel: Ein Array mit Rollen, welche den Benutzer zur Ausführung der geschützten Aktion berechtigen, ist bekannt.

#### <a name="step4"></a>Werte des relevanten Attributs für den aktiven Benutzer bestimmen
Aufgrund der vorhergehenden Evaluationen soll nun die Menge der zulässigen Werte für das relevante Attribut bestimmt werden. Dazu werden die verfügbaren Attribut-Werte des aktiven Benutzers gefiltert, so dass nur jene Attribut-Werte in der Resultatmenge verbleiben, welche
+ dem Attribut-Identifier aus dem Abschnitt [Attribut festlegen](#step1) entsprechen
+ einer einer Rolle aus der im Abschnitt [Relevante Rollen zur Ausführung der geschützten Aktion bestimmen](#step3) bestimmten Menge entsprechen

Die evaluierten Attribut-Werte sollen in aufbereiteter Form, d.h. als Repräsentation gemäss ihrem Attributs-Typ, als Ergebnis zur Verfügung stehen.

> Ziel: Eine Sammlung mit Attribut-Werten gemäss definiertem Attribut-Typ ist bekannt.

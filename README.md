# Workshop Vorbereitungen
In diesem File werden Gedanken, Fragen und Verweise für mögliche Workshop-Themen und -Fragen festgehalten. Alle Anliegen sind so gut wie möglich mit Code-Beispielen zu illustrieren!

## Inhalt
0. [Factories in Objects.yaml](#i1)
0. [...](#i2)

## <a name="i1">Factories in Objects.yaml
Im Abschnitt [Custom Factories](http://flowframework.readthedocs.io/en/stable/TheDefinitiveGuide/PartIII/ObjectManagement.html#custom-factories "Custom Factories in der Flow Dokumentation") der Flow Dokumentation wird beschrieben, wie Factories für komplexe Objekte definiert werden können. Mir ist unklar, inwiefern diese Definition in Objects.yaml die Erstellung von Objekten beeinflusst.

=> Ist es relevant, **wie** (_new_ keyword, ObjectManager, Aufruf der Factory Methode) solche Objekte erzeugt werden?

_Beispiele: Factories für Policy-Objekte Role und Attribute._

## <a name="i2">...
...